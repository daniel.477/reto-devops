# Modo de uso


Se realiza la tarea 1, para ejecutarla en modo local se debe hacer lo siguiente:

1. Clonar el proyecto y posicionarse:
```bash
git clone https://gitlab.com/daniel.477/reto-devops.git && cd reto-devops
```

2. Levantar el docker con el siguiente comando:
```bash
docker-compose up
```
3. Una vez arriba y ejecutadas las pruebas unitarias, debemos identificar el id de docker (**CONTAINER ID**) que esta corriendo con el comando:
```bash
docker ps
```

4. Luego debemos entrar a la terminal del docker levantado, para eso debemos copiar el id del docker o completar con los primeros 4 caracteres de su id en el siguiente comando:
```bash
docker exec -it <id docker>  /bin/sh
```

5. Ya dentro del terminal podemos ejecutar los comando de la api:
```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

