FROM node:10.23.1-alpine3.9

#se instala git, curl, jq
RUN apk update
RUN apk add git
RUN apk add curl
RUN apk add jq

#creamos carpeta del proyecto
RUN mkdir -p /home/node/ && chown -R node:node /home/node

#usamos usuario node de la imagen y establecemos usuario node como usuario de trabajo
USER node
WORKDIR /home/node

#clonamos repo CLM
RUN git clone https://gitlab.com/clm-public/reto-devops.git  	 

#configuramos el proyecto e iniciamos	 
RUN  cd ./reto-devops                                                       \
     && npm install                                                   \
     && npm run test                                                    \
	 && node index.js	
	 
EXPOSE 3000